include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_BINARY_DIR}/src
    ${CMAKE_SOURCE_DIR}/src/geom/cgm
    ${CMAKE_SOURCE_DIR}/src/util/cgm
    ${CMAKE_BINARY_DIR}/src/util/cgm
    ${CMAKE_SOURCE_DIR}/src/init/cgm
    ${CMAKE_BINARY_DIR}/src/init/cgm
    ${CMAKE_BINARY_DIR}/src/geom/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/virtual/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/facet/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/Cholla/cgm
    ${CMAKE_SOURCE_DIR}/src/init/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/OCC/cgm )

set(TEST_COMP_FLAGS "-DTEST -DSRCDIR=${CMAKE_SOURCE_DIR}/test ${CGMA_OCC_DEFINES}")
if(ENABLE_OCC)
  set(TEST_COMP_FLAGS "-DTEST_OCC -DHAVE_OCC ${TEST_COMP_FLAGS}")
endif(ENABLE_OCC)

add_library(cgm_test TestUtilities.cpp)
target_link_libraries(cgm_test PUBLIC cgm PRIVATE ${CGM_LIBS} ${extra_libs} )

set( TESTS init.cpp
           facets.cpp
           fileoptions.cpp
           test_stl.cpp
           )

get_property(CGM_LIBS GLOBAL
  PROPERTY cgm_libs)
get_property(CGM_DEFINES GLOBAL
  PROPERTY cgm_defines)
get_property(CGM_DEPLIBS GLOBAL
  PROPERTY cgm_deplibs)

foreach( fname ${TESTS} )
  string( REPLACE ".cpp" "" tmp ${fname} )
  string( REPLACE ".cxx" "" base ${tmp} )
  add_executable( ${base} ${fname})
  set_target_properties( ${base} PROPERTIES COMPILE_FLAGS "${TEST_COMP_FLAGS} ${CGM_DEFINES}" )
  target_link_libraries( ${base} PUBLIC cgm_test cgm ${CGM_DEPLIBS} ${CGM_LIBS} ${extra_libs} )
  add_test( ${base} ${EXECUTABLE_OUTPUT_PATH}/${base} )
endforeach()
 
if(ENABLE_OCC)
  set(TEST_COMP_FLAGS_OCC "-DTEST_OCC -DHAVE_OCC ${TEST_COMP_FLAGS} ${CGMA_OCC_DEFINES}")
  set( TESTS_OCC
             sheet.cpp
             edgeFaceSense.cpp
             raySurfaceInt.cpp
             attribute_to_file.cpp
             loft.cpp
             offset_curves.cpp
             point_project.cpp
             # imprint_bug.cpp
             # subtract.cpp
             test_occ.cpp
             # operation.cpp
             section.cpp
             AngleCalc.cpp
             CreateGeometry.cpp
             GraphicsData.cpp
             brick.cpp
             spheres.cpp
             cylinders.cpp
             multifaceted.cpp
             multifaceted-works.cxx
             multifaceted-2trisPerSurface-works.cxx
             split_circle.cpp
             merge_test.cpp
             r_w.cpp
             groups-saved-to-OCC.cxx
             scale.cpp
              )

  foreach( fname ${TESTS_OCC} )
    string( REPLACE ".cpp" "" tmp ${fname} )
    string( REPLACE ".cxx" "" base ${tmp} )
    add_executable( ${base} ${fname})
    set_target_properties( ${base} PROPERTIES COMPILE_FLAGS "${TEST_COMP_FLAGS_OCC} ${CGM_DEFINES} '-DTEST_ENGINE=\"OCC\"'" )
    target_link_libraries( ${base} PUBLIC cgm_test cgm PRIVATE ${CGM_LIBS} ${extra_libs} ${OCC_LIBRARIES} ${OCE_LIBRARIES} )
    add_test( ${base} ${EXECUTABLE_OUTPUT_PATH}/${base} )
  endforeach()

endif()

  set(TEST_COMP_FLAGS_FACET "-DTEST_FACET -DHAVE_FACET ${TEST_COMP_FLAGS} '-DTEST_ENGINE=\"FACET\"'")

  add_executable( brick_facet brick.cpp)
  set_target_properties( brick_facet PROPERTIES COMPILE_FLAGS "${TEST_COMP_FLAGS_FACET} ${CGM_DEFINES}" )
  target_link_libraries( brick_facet PUBLIC cgm_test cgm PRIVATE ${CGM_LIBS} ${extra_libs} )
  add_test( brick_facet ${EXECUTABLE_OUTPUT_PATH}/brick_facet )

  add_executable( merge_facet merge_test.cpp)
  set_target_properties( merge_facet PROPERTIES COMPILE_FLAGS "${TEST_COMP_FLAGS_FACET} ${CGM_DEFINES}" )
  target_link_libraries( merge_facet PUBLIC cgm_test cgm PRIVATE ${CGM_LIBS} ${extra_libs} )
  add_test( merge_facet ${EXECUTABLE_OUTPUT_PATH}/merge_facet )

