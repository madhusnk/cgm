project(cgm_parallel)

if (USE_MPI)
  find_package(MPI REQUIRED)
endif ()

set(cgm_parallel_srcs
  CABodies.cpp
  CGMProcConfig.cpp
  TDParallel.cpp)

set(cgm_parallel_headers
  cgm/CABodies.hpp
  cgm/CGMParallelConventions.h
  cgm/CGMProcConfig.hpp
  cgm/CGMmpi.h
  cgm/CGMmpi_config.h
  cgm/TDParallel.hpp)

if (USE_MPI)
  list(APPEND cgm_parallel_srcs
    CGMParallelComm.cpp
    CGMReadParallel.cpp)
  list(APPEND cgm_parallel_headers
    cgm/CGMParallelComm.hpp
    cgm/CGMReadParallel.hpp)
endif ()

cgm_source_interface(all_cgm_parallel_srcs
  cgm_parallel_srcs
  cgm_parallel_headers)

include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}/cgm
  ${CMAKE_CURRENT_BINARY_DIR}/cgm
  ${CMAKE_SOURCE_DIR}/src/geom/cgm
  ${CMAKE_BINARY_DIR}/src/geom/cgm
  ${CMAKE_SOURCE_DIR}/src/util/cgm
  ${CMAKE_BINARY_DIR}/src/util/cgm
)

cgm_add_library(cgm_parallel 1
    ${all_cgm_parallel_srcs})

target_link_libraries(cgm_parallel
  PUBLIC
    cgm_geom
    cgm_util
  )

if (USE_MPI)
  target_compile_definitions(cgm_parallel
    PUBLIC
      USE_MPI)
  include_directories(
      ${MPI_CXX_INCLUDE_PATH})
  target_link_libraries(cgm_parallel
    PUBLIC
      ${MPI_CXX_LIBRARIES})
endif ()

install(
  TARGETS   "cgm_parallel"
  EXPORT    cgm_geom
  RUNTIME   DESTINATION bin
  LIBRARY   DESTINATION lib
  ARCHIVE   DESTINATION lib
  COMPONENT Runtime)

cgm_install_headers(${cgm_parallel_headers})

