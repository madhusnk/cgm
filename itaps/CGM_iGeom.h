#ifndef CGM_IGEOM_H
#define CGM_IGEOM_H

#include "CGMIGeomConfigure.h"

#ifdef __cplusplus
extern "C" {
#endif

  CGMA_IGEOM_EXPORT
  void CGM_iGeom_register();

#ifdef __cplusplus
}
#endif


#endif
